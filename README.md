# Kata: Largest Difference in Array

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Create a method that takes in an integer array anf find the maximum difference in that array. Provided the minimum number appears before the maximum number in the array. You then need to find elements where the combination of two elements equal that maximum, no duplicate counts. The solution is implemented in C# using a Console Application in Visual Studio.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

- Clone 
- Open in Visual Studio
- Build the application

## Usage

- Run the application from Visual Studio.

## Maintainers

[Nicholas Lennox](https://github.com/NicholasLennox)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2021 Noroff Accelerate AS
