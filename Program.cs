﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kata2_LargestDifference
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 2, 3, 1, 7, 9, 5, 11, 3, 5 };
            int maxDiff = FindLargestDifference(numbers);
            Console.WriteLine(maxDiff);
            Console.WriteLine(NumbersOfPairsWithMaxDifference(numbers, maxDiff));
        }


        private static int FindLargestDifference(int[] numbers)
        {
            // Setting the starting max difference to be the
            // difference between the first two elements.
            // Min is set as the first element.
            int maxDiff = numbers[1] - numbers[0];
            int minElement = numbers[0];
            // We can start at index 1 because we have done the first index's calculations
            for (int i = 1; i < numbers.Length; i++)
            {
                // If current difference is larger than our previous max
                if (numbers[i] - minElement > maxDiff)
                {
                    maxDiff = numbers[i] - minElement;
                }  else if (numbers[i] < minElement) // If the current element is our new min
                {
                    // This way we always ensure our min is behind our current element.
                    minElement = numbers[i];
                }
            }
            return maxDiff;
        }

        // Doing it with nested loops to mix it up
        // Could have done the "find max" this way too. 
        private static int NumbersOfPairsWithMaxDifference(int[] numbers, int maxDiff)
        {
            int count = 0;
            // Start at current element and compare every element after it
            for (int i = 0; i < numbers.Length; i++)
            {
                for (int j = i + 1; j < numbers.Length; j++)
                {
                    // Add our starting and current element together to compare to max
                    if (numbers[j] + numbers[i] == maxDiff)
                    {
                        count++;
                    }
                }
            }
            return count;
        }


    }
}
